<?php

/**
 * @file
 * Lists available colors and color schemes for the my_theme theme.
 */

$info = [
  // Available colors and color labels used in theme.
  'fields' => [
    'color-1' => t('Color 1'),
    'color-1-12' => t('Color 1 with transparency 12%'),
    'color-1-30' => t('Color 1 with transparency 30%'),
    'color-1-50' => t('Color 1 with transparency 50%'),
    'color-2' => t('Color 2'),
    'color-2-60' => t('Color 2 with transparency 60%'),
    'color-3' => t('Color 3'),
    'color-4' => t('Color 4'),
    'color-5' => t('Color 5'),
    'color-6' => t('Color 6'),
    'color-7' => t('Color 7'),
    'color-8' => t('Color 8'),
    'color-9' => t('Color 9'),
    'color-10' => t('Color 10'),
    'color-11' => t('Color 11'),
    'color-11-50' => t('Color 11 with transparency 50%'),
    'color-11-90' => t('Color 11 with transparency 90%'),
    'color-12' => t('Color 12'),
    'color-12-80' => t('Color 12 with transparency 80%'),
    'color-13' => t('Color 13'),
    'color-14' => t('Color 14'),
    'color-15' => t('Color 15'),
  ],
  // Pre-defined color schemes.
  'schemes' => [
    'default' => [
      'title' => t('Functional Palette'),
      'colors' => [
        'color-1' => '#000000',
        'color-1-12' => 'rgba(0, 0, 0, 0.12)',
        'color-1-30' => 'rgba(0, 0, 0, 0.3)',
        'color-1-50' => 'rgba(0, 0, 0, 0.5)',
        'color-2' => '#ffffff',
        'color-2-60' => 'rgba(255, 255, 255, 0.6)',
        'color-3' => '#f8f8f8',
        'color-4' => '#dddddd',
        'color-5' => '#c6c6c6',
        'color-6' => '#9b9b9b',
        'color-7' => '#4a4a4a',
        'color-8' => '#222222',
        'color-9' => '#0f3a58',
        'color-10' => '#0c64a4',
        'color-11' => '#00bee6',
        'color-11-50' => 'rgba(0, 190, 230, 0.5)',
        'color-11-90' => 'rgba(0, 190, 230, 0.9)',
        'color-12' => '#2cbb17',
        'color-12-80' => 'rgba(44, 187, 23, 0.8)',
        'color-13' => '#e6007e',
        'color-14' => '#ff1a98',
        'color-15' => '#777777',
      ],
    ],
  ],

  // CSS files (excluding @import) to rewrite with new color scheme.
  'css' => [
    'color/colors.css',
  ],

  'preview_library' => 'my_theme/color.preview',
  'preview_html' => 'color/preview.html',

  // Files to copy.
  'copy' => [
    'logo.svg',
  ],
];
