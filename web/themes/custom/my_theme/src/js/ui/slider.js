import applyBannerSlider from '@my_theme/components/organisms/banner-slider/o-banner-slider';
import applyTeaserSlider from '@my_theme/components/organisms/teaser-slider/o-teaser-slider';

(({ behaviors }) => {
  behaviors.my_themeBannerSlider = {
    attach: context => {
      applyBannerSlider({
        context,
      });
    },
  };

  behaviors.my_themeTeaserSlider = {
    attach: context => {
      const landingParagraphs = context.querySelector(
        '.node--type-landing-page.node--view-mode-full.overlap-with-paragraph .field--name-paragraph',
      );
      if (landingParagraphs) {
        // On landing page for common slider we need other options
        // in case if "overlap:true" field enabled on node.
        // Between 1024-1440 there should be 3 columns in slider, not 4.
        // We need such behavior only if paragraph with slider has 1st position
        // in the list of paragraphs, or 2nd position after small paragraphs.
        // Small paragraphs - paragraphs with small height.
        // List of small paragraphs:
        //   1. Paragraph "Wysiwyg"
        //   2. Paragraph "Widget-search"
        Array.prototype.forEach.call(
          landingParagraphs.querySelectorAll(
            '.field__paragraph--list-of-teasers:first-child,' +
              '.field__paragraph--widget-search:first-child + .field__paragraph--list-of-teasers,' +
              '.field__paragraph--wysiwyg:first-child + .field__paragraph--list-of-teasers',
          ),
          el => {
            const slider = el.querySelector('.o-teaser-slider');
            slider.classList.add('o-teaser-slider--in-small-container');
            slider.classList.remove('o-teaser-slider--default');
            applyTeaserSlider({
              context: el,
              modifier: 'in-small-container',
              options: {
                slidesPerView: 4,
                spaceBetween: 30,
                watchOverflow: true,
                pagination: {
                  clickable: true,
                  bulletClass: 'a-slider-pagination__bullet',
                  bulletActiveClass: 'a-slider-pagination__bullet--active',
                  clickableClass: 'a-slider-pagination--clickable',
                },
                navigation: {
                  disabledClass: 'a-slider-arrow--disabled',
                },
                breakpoints: {
                  1439: {
                    slidesPerView: 3,
                  },
                  1023: {
                    slidesPerView: 1.35,
                  },
                },
              },
            });
          },
        );

        Array.prototype.forEach.call(
          landingParagraphs.querySelectorAll(
            '.field__paragraph--list-of-teasers-with-highlights:first-child,' +
              '.field__paragraph--widget-search:first-child + .field__paragraph--list-of-teasers-with-highlights,' +
              '.field__paragraph--wysiwyg:first-child + .field__paragraph--list-of-teasers-with-highlights',
          ),
          el => {
            el.querySelector('.o-teaser-slider--with-highlights').classList.add(
              'o-teaser-slider--with-highlights--in-small-container',
            );
          },
        );
      }
      applyTeaserSlider({
        context,
      });
      applyTeaserSlider({
        context,
        modifier: 'with-highlights',
        options: {
          slidesPerView: 'auto',
          spaceBetween: 30,
          watchOverflow: true,
          pagination: {
            clickable: true,
            bulletClass: 'a-slider-pagination__bullet',
            bulletActiveClass: 'a-slider-pagination__bullet--active',
            clickableClass: 'a-slider-pagination--clickable',
          },
          navigation: {
            disabledClass: 'a-slider-arrow--disabled',
          },
          breakpoints: {
            1023: {
              slidesPerView: 1.35,
            },
          },
        },
      });
    },
  };
})(Drupal);
