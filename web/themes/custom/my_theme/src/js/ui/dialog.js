/**
 * @file
 * This is jQueryUI dialog custom functionality.
 */

(({ behaviors }, $, drupalSettings) => {
  behaviors.my_themeDialog = {
    attach: context => {
      $(context)
        .once('my_theme-dialog-processed')
        .on({
          dialogcreate: () => {
            $(context)
              .find('.ui-dialog-titlebar-close')
              .html(() => {
                return `<svg aria-hidden="true">
                      <use xlink:href="${drupalSettings.my_themeSvgSpritePath}#svg-cross"></use>
                    </svg>`;
              });
          },
        });
    },
  };
})(Drupal, jQuery, drupalSettings);
