import applySelect from '@my_theme/components/atoms/select/select';

(({ behaviors }, $) => {
  behaviors.my_themeSelect = {
    attach: context => {
      applySelect({
        context,
        $,
      });
    },
  };
})(Drupal, jQuery);
