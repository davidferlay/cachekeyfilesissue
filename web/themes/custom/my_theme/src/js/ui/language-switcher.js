const toggleLS = (ls, btn, ddown) => {
  if (!ls.classList.contains('lang-switcher--active')) {
    ls.classList.add('lang-switcher--active');
    btn.classList.add('lang-switcher__button--active');
    btn
      .querySelector('.lang-switcher__button-arrow')
      .classList.add('lang-switcher__button-arrow--active');
    ddown.classList.add('lang-switcher__dropdown--active');
  } else {
    ls.classList.remove('lang-switcher--active');
    btn.classList.remove('lang-switcher__button--active');
    btn
      .querySelector('.lang-switcher__button-arrow')
      .classList.remove('lang-switcher__button-arrow--active');
    ddown.classList.remove('lang-switcher__dropdown--active');
  }
};

(({ behaviors }) => {
  behaviors.my_themeLanguageSwitcher = {
    attach: context => {
      const ls = context.querySelector('.lang-switcher');

      if (ls) {
        const btn = ls.querySelector('.lang-switcher__button');
        const ddown = ls.querySelector('.lang-switcher__dropdown');

        btn.addEventListener('click', () => {
          toggleLS(ls, btn, ddown);
        });

        document.addEventListener('click', e => {
          if (ls.classList.contains('lang-switcher--active')) {
            if (e.target.closest('.lang-switcher')) return;

            toggleLS(ls, btn, ddown);
          }
        });
      }
    },
  };
})(Drupal);
