/**
 * @file
 * This is burger-menu component.
 */

import { disableBodyScroll, enableBodyScroll } from 'body-scroll-lock';
import { addBreakpointListener } from '../breakpoints';

const openSideNav = (sideMenu, openSideNavBtn, closeSideNavBtn, overlay) => {
  openSideNavBtn.setAttribute('aria-expanded', 'true');
  closeSideNavBtn.setAttribute('aria-expanded', 'true');
  overlay.classList.add('l-site-template__overlay--active');
  sideMenu.classList.add('l-site-template__side-nav--active');
  disableBodyScroll(sideMenu);
};

const closeSideNav = (sideMenu, openSideNavBtn, closeSideNavBtn, overlay) => {
  openSideNavBtn.setAttribute('aria-expanded', 'false');
  closeSideNavBtn.setAttribute('aria-expanded', 'false');
  overlay.classList.remove('l-site-template__overlay--active');
  sideMenu.classList.remove('l-site-template__side-nav--active');
  enableBodyScroll(sideMenu);
};

(({ behaviors }) => {
  behaviors.my_themeHeader = {
    attach: context => {
      const sideMenu = context.querySelector('.l-site-template__side-nav');
      if (sideMenu) {
        const openSideNavBtn = context.querySelector(
          '.l-site-template__iconic-button--burger',
        );
        const closeSideNavBtn = context.querySelector(
          '.l-site-template__iconic-button--cross',
        );
        const overlay = context.querySelector('.l-site-template__overlay');
        openSideNavBtn.addEventListener('click', () => {
          if (
            !sideMenu.classList.contains('l-site-template__side-nav--active')
          ) {
            openSideNav(sideMenu, openSideNavBtn, closeSideNavBtn, overlay);
          } else {
            closeSideNav(sideMenu, openSideNavBtn, closeSideNavBtn, overlay);
          }
        });
        closeSideNavBtn.addEventListener('click', () => {
          if (
            !sideMenu.classList.contains('l-site-template__side-nav--active')
          ) {
            openSideNav(sideMenu, openSideNavBtn, closeSideNavBtn, overlay);
          } else {
            closeSideNav(sideMenu, openSideNavBtn, closeSideNavBtn, overlay);
          }
        });

        addBreakpointListener(
          'wide_1x',
          () => {
            if (
              sideMenu.classList.contains('l-site-template__side-nav--active')
            ) {
              closeSideNav(sideMenu, openSideNavBtn, closeSideNavBtn, overlay);
            }
          },
          100,
        );
      }
    },
  };
})(Drupal);
