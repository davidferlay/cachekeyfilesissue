import { addBreakpointListener } from '../breakpoints';

(({ behaviors }, $) => {
  behaviors.my_themeSearchFormCustomized = {
    attach: (context, settings) => {
      const icon = `<svg aria-hidden="true" class="o-modal__icon"><use xlink:href="${settings.my_themeSvgSpritePath}#svg-arrow--long"></use></svg>`;

      Array.prototype.forEach.call(
        context.querySelectorAll(
          `.js-my_theme-widget-search-form:not(.search-form-theme-processed)`,
        ),
        el => {
          const $form = $(el).find('form');
          const $autocomplete = $form.find('.js-field-destination');
          const $startDatePicker = $form.find('.js-field-date-start');
          const $endDatePicker = $form.find('.js-field-date-end');

          const enableMobile = () => {
            $autocomplete.off('focus');
            $autocomplete.on('focus', () => {
              if (!$autocomplete.hasClass('ui-dialog-popup-processed')) {
                $autocomplete.addClass('ui-dialog-popup-processed');
                $autocomplete.parent().dialog({
                  classes: {
                    'ui-dialog': 'o-modal',
                    'ui-dialog-titlebar': 'o-modal__header',
                    'ui-dialog-title': 'o-modal__title',
                    'ui-dialog-titlebar-close':
                      'o-modal__close o-modal__close--left',
                    'ui-dialog-content': 'o-modal__content',
                  },
                  create: () => {
                    $('body').css({
                      overflow: 'hidden',
                    });
                  },
                  beforeClose: () => {
                    $('body').css({
                      overflow: 'inherit',
                    });
                  },
                  modal: true,
                  title: Drupal.t('Choose a destination'),
                  width: window.innerWidth,
                  close: () => {
                    $autocomplete.parent().dialog('destroy');
                  },
                  open: () => {
                    $('.o-modal__close').html(icon);
                    if (
                      !$autocomplete.hasClass('ui-autocomplete-input--enabled')
                    ) {
                      $autocomplete.autocomplete({
                        appendTo: '.ui-dialog-content',
                        close: () => {
                          $autocomplete.parent().dialog('close');
                          setTimeout(() => {
                            $autocomplete.removeClass(
                              'ui-dialog-popup-processed',
                            );
                            $autocomplete.blur();
                          }, 50);
                        },
                      });

                      $autocomplete.autocomplete('search', '');
                      $autocomplete.addClass('ui-autocomplete-input--enabled');
                    }

                    if (
                      !$('.ui-dialog-content .ui-autocomplete').is(':visible')
                    ) {
                      $('.ui-dialog-content .ui-autocomplete').show();
                    }
                  },
                });
              }
            });

            $startDatePicker.datepicker('option', 'numberOfMonths', 1);
            $endDatePicker.datepicker('option', 'numberOfMonths', 1);
          };

          const enableDesktop = () => {
            $autocomplete.off('focus');
            $autocomplete.autocomplete('option', 'appendTo', null);
            $autocomplete.on('focus', () => {
              $autocomplete.autocomplete('search', '');
              $autocomplete.on('autocompleteclose', () => {
                $autocomplete.blur();
              });
            });
            $startDatePicker.datepicker('option', 'numberOfMonths', 2);
            $endDatePicker.datepicker('option', 'numberOfMonths', 2);
          };

          addBreakpointListener(
            'mobile_1x',
            () => {
              enableMobile();
            },
            100,
          );

          addBreakpointListener(
            'narrow_1x',
            () => {
              enableDesktop();
            },
            100,
          );

          addBreakpointListener(
            'wide_1x',
            () => {
              enableDesktop();
            },
            100,
          );

          el.classList.add('search-form-theme-processed');
        },
      );
    },
  };
})(Drupal, jQuery);
