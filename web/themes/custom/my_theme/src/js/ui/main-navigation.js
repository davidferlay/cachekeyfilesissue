/**
 * @file
 * This is main-navigation component.
 */

import { addBreakpointListener } from '../breakpoints';

(({ behaviors }) => {
  behaviors.my_themetMainMenu = {
    attach(context) {
      Array.prototype.forEach.call(
        context.querySelectorAll('.menu--main .menu-item__container--expanded'),
        expandedItem => {
          expandedItem.addEventListener('click', () => {
            if (
              !expandedItem
                .closest('.menu-item--expanded')
                .classList.contains('menu-item--active')
            ) {
              expandedItem
                .closest('.menu-item--expanded')
                .classList.add('menu-item--active');
            } else {
              expandedItem
                .closest('.menu-item--expanded')
                .classList.remove('menu-item--active');

              Array.prototype.forEach.call(
                expandedItem
                  .closest('.menu-item--expanded')
                  .querySelectorAll('.menu-item--active'),
                activeChild => {
                  activeChild.classList.remove('menu-item--active');
                },
              );
            }
          });
        },
      );

      Array.prototype.forEach.call(
        context.querySelectorAll(
          '.menu--main .menu-item__container--expanded > a',
        ),
        expandedLink => {
          expandedLink.classList.add('click-disabled');
          expandedLink.addEventListener('click', e => {
            e.preventDefault();
          });
        },
      );

      addBreakpointListener(
        'wide_1x',
        () => {
          Array.prototype.forEach.call(
            context.querySelectorAll('.menu--main .menu-item--active'),
            activeMenu => {
              activeMenu.classList.remove('menu-item--active');
            },
          );
        },
        100,
      );
    },
  };
})(Drupal);
