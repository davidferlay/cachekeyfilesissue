// Full list of supported options can be found in [config-all.json](https://github.com/Modernizr/Modernizr/blob/master/lib/config-all.json).

'use strict';

module.exports = {
  'options': [
    'testAllProps',
    'setClasses'
  ],
  'feature-detects': [
    'test/css/cssgrid',
  ]
};
