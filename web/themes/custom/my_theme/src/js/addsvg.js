/**
 * @file
 * This is js file to insert svg sprites to the selector.
 */

const svgOptions = [
  {
    selector:
      '.block-region-eighth-region .block-content--paragraph + .block-content--paragraph .field--name-paragraph > .field__item:first-child .field--name-wysiwyg',
    svg: 'phone',
    where: 'before',
  },
  {
    selector:
      '.block-region-eighth-region .block-content--paragraph + .block-content--paragraph .field--name-paragraph > .field__item:last-child .field--name-wysiwyg',
    svg: 'mail',
    where: 'before',
  },
];

(({ behaviors }) => {
  behaviors.addSvg = {
    attach(context, settings) {
      for (let i = 0; i < svgOptions.length; i++) {
        Array.prototype.forEach.call(
          context.querySelectorAll(
            `${svgOptions[i].selector}:not(.svg-processed)`,
          ),
          el => {
            let whereToAdd;
            if (svgOptions[i].where === 'before') {
              whereToAdd = 'afterbegin';
            } else {
              whereToAdd = 'beforeend';
            }
            el.insertAdjacentHTML(
              whereToAdd,
              `<svg aria-hidden="true" class="js-svg-${svgOptions[i].svg}-attached">
                <use xlink:href="${settings.my_themeSvgSpritePath}#svg-${svgOptions[i].svg}"></use>
              </svg>`,
            );
            el.classList.add('svg-processed');
          },
        );
      }
    },
  };
})(Drupal);
