import '@babel/polyfill';
import 'custom-event-polyfill';
import cssVars from 'css-vars-ponyfill';
import elementClosest from 'element-closest';
import svg4everybody from 'svg4everybody';
import modernizr from './.modernizrrc';
import { initBreakpointsCssReload } from './breakpoints';
import './svg-sprite';
import './ui/dialog';
import './ui/header';
import './ui/language-switcher';
import './ui/search-form';
import './ui/main-navigation';
import './ui/select';
import './ui/slider';
import './addsvg';

// TODO: Check if css classes need to be reloaded by default.
// https://i.gyazo.com/67cc358aca4e7216e587a740b9f96b99.gif
initBreakpointsCssReload();
elementClosest(window);
svg4everybody();

(({ behaviors }) => {
  behaviors.my_themeVarsPolyfill = {
    attach: () => {
      // Add css-vars-ponyfill if it's not support.
      if (!modernizr.testAllProps('customproperties')) {
        cssVars({
          onComplete() {
            // Added trigger 'resize' event, when cssvars is completed.
            const resizeEvent = window.document.createEvent('UIEvents');
            resizeEvent.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(resizeEvent);
          },
        });
      }
    },
  };
})(Drupal);
