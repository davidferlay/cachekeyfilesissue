import { storiesOf } from '@storybook/html';
import './button.css';
import svg from '../../../../dist/svg/sprite.svg';

const template = require('./a-button.html.twig');
const data = require('./a-button.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('atoms|button', module)
  .add('DEFAULT', () => template(data))
  .add('DEFAULT icon left', () =>
    template({
      ...data,
      svg,
      svg_before: 1,
    }),
  )
  .add('DEFAULT icon right', () =>
    template({
      ...data,
      svg,
      svg_after: 1,
    }),
  )
  .add('SMALL', () =>
    template({
      ...data,
      button_modifiers: 'a-button--small',
    }),
  )
  .add('SMALL icon left', () =>
    template({
      ...data,
      svg,
      svg_before: 1,
      button_modifiers: 'a-button--small',
      svg_modifiers: 'a-button__icon--small a-button__icon--left-side--small',
    }),
  )
  .add('SMALL icon right', () =>
    template({
      ...data,
      svg,
      svg_after: 1,
      button_modifiers: 'a-button--small',
      svg_modifiers: 'a-button__icon--small a-button__icon--right-side--small',
    }),
  )
  .add('SMALL-ON-MOBILE', () =>
    template({
      ...data,
      button_modifiers: 'a-button--small-on-mobile',
    }),
  )
  .add('SMALL-ON-MOBILE icon left', () =>
    template({
      ...data,
      svg,
      svg_before: 1,
      button_modifiers: 'a-button--small-on-mobile',
      svg_modifiers:
        'a-button__icon--small-on-mobile a-button__icon--left-side--small-on-mobile',
    }),
  )
  .add('SMALL-ON-MOBILE icon right', () =>
    template({
      ...data,
      svg,
      svg_after: 1,
      button_modifiers: 'a-button--small-on-mobile',
      svg_modifiers:
        'a-button__icon--small-on-mobile a-button__icon--right-side--small-on-mobile',
    }),
  )
  .add('SMALLEST', () =>
    template({
      ...data,
      button_modifiers: 'a-button--smallest',
    }),
  )
  .add('SMALLEST icon left', () =>
    template({
      ...data,
      svg,
      svg_before: 1,
      button_modifiers: 'a-button--smallest',
      svg_modifiers:
        'a-button__icon--smallest a-button__icon--left-side--smallest',
    }),
  )
  .add('SMALLEST icon right', () =>
    template({
      ...data,
      svg,
      svg_after: 1,
      button_modifiers: 'a-button--smallest',
      svg_modifiers:
        'a-button__icon--smallest a-button__icon--right-side--smallest',
    }),
  )
  .add('VARIANT: more round', () =>
    template({
      ...data,
      button_modifiers: 'a-button--more-round',
    }),
  )
  .add('VARIANT: no round', () =>
    template({
      ...data,
      button_modifiers: 'a-button--no-round',
    }),
  )
  .add('VARIANT: color reversed', () =>
    template({
      ...data,
      button_modifiers: 'a-button--color--reversed',
    }),
  )
  .add('VARIANT: color secondary', () =>
    template({
      ...data,
      button_modifiers: 'a-button--color--secondary',
    }),
  )
  .add('VARIANT: transition default', () =>
    template({
      ...data,
      button_modifiers: 'a-button--transition a-button--transition--default',
    }),
  );
