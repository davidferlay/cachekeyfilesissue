import { storiesOf } from '@storybook/html';
import './slider-arrow.css';

import svg from '../../../../dist/svg/sprite.svg';

const template = require('./a-slider-arrow.html.twig');
const data = require('./a-slider-arrow.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('atoms|slider-arrow', module)
  .add('default', () =>
    template({
      ...data,
      svg,
    }),
  )
  .add('secondary', () =>
    template({
      ...data,
      svg,
      modifier_class: 'a-slider-arrow--secondary',
      icon_modifier_class: 'a-slider-arrow__icon--secondary',
    }),
  );
