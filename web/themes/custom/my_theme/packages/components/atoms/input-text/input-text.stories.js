/**
 * @file
 * Stories.
 */

import { storiesOf } from '@storybook/html';
import './input-text.css';


const template = require('./a-input-text.html.twig');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('atoms|input-text', module)
  .add('default', () => template())
  .add('secondary', () =>
    template({
      modifier_class: 'a-input-text--secondary',
    }),
  );

