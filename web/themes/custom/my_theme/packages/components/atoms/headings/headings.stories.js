import { storiesOf } from '@storybook/html';
import './headings.css';


const template = require('./a-headings.html.twig');
const data = require('./a-headings.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('atoms|headings', module).add('default', () => template(data));

