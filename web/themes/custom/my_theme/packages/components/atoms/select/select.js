const defaultOptions = {
  classes: {
    'ui-selectmenu-button': 'a-input-text',
    'ui-selectmenu-menu': 'a-select__menu',
  },
};

export default ({
  className = '.a-select__box',
  classProcessed = 'a-select--processed',
  context = document,
  options = defaultOptions,
  $ = window.jQuery,
} = {}) => {
  Array.prototype.forEach.call(
    context.querySelectorAll(`${className}:not(.${classProcessed})`),
    el => {
      el.classList.add(classProcessed);
      $(el).selectmenu(options);
    },
  );
};
