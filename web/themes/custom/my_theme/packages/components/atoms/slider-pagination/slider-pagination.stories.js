import { storiesOf } from '@storybook/html';
import './slider-pagination.css';

const template = require('./a-slider-pagination.html.twig');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('atoms|slider-pagination', module)
  .add('default', () => template())
  .add('clickable', () => template({
    modifier_class: 'a-slider-pagination--clickable',
  }));
