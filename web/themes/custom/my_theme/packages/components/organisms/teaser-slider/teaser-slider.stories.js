import { storiesOf } from '@storybook/html';
import './teaser-slider.css';
import teaserSlider from './o-teaser-slider';
import svg from '../../../../dist/svg/sprite.svg';

const template = require('./o-teaser-slider.html.twig');
const data = require('./o-teaser-slider.json');

const itemTemplate = require('../../molecules/teaser/m-teaser.html.twig');
const itemData = require('../../molecules/teaser/m-teaser.json');

itemData.modifier_class = 'o-banner-slider__item';
const firstItem = itemTemplate({
  ...itemData,
  modifier_class: 'm-teaser--banner',
});
const item = itemTemplate(itemData);
data.content.items = [item, item, item];

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('organisms|teaser-slider', module)
  .add('default', () => {
    document.addEventListener(
      'DOMNodeInserted',
      () => {
        teaserSlider();
      },
      false,
    );
    return template({
      ...data,
      svg,
    });
  })
  .add('with banner', () => {
    document.addEventListener(
      'DOMNodeInserted',
      () => {
        teaserSlider();
      },
      false,
    );
    return template({
      ...data,
      svg,
    });
  });
