/**
 * @file
 * This is component sctipt template.
 */

import { Swiper, Navigation, Pagination } from 'swiper/dist/js/swiper.esm';

Swiper.use([Navigation, Pagination]);

const defaultOptions = {
  slidesPerView: 4,
  spaceBetween: 30,
  watchOverflow: true,
  pagination: {
    clickable: true,
    bulletClass: 'a-slider-pagination__bullet',
    bulletActiveClass: 'a-slider-pagination__bullet--active',
    clickableClass: 'a-slider-pagination--clickable',
  },
  navigation: {
    disabledClass: 'a-slider-arrow--disabled',
  },
  breakpoints: {
    1023: {
      slidesPerView: 1.35,
    },
  },
};

export default ({
  className = 'o-teaser-slider',
  modifier = 'default',
  classProcessed = 'o-teaser-slider--processed',
  context = document,
  options = defaultOptions,
} = {}) => {
  Array.prototype.forEach.call(
    context.querySelectorAll(
      `.${className}--${modifier}:not(.${classProcessed})`,
    ),
    el => {
      options = {
        ...options,
        navigation: {
          ...options.navigation,
          nextEl: el.querySelector(`.${className}__arrow--right`),
          prevEl: el.querySelector(`.${className}__arrow--left`),
        },
        pagination: {
          ...options.pagination,
          el: el.querySelector(`.${className}__pagination`),
        },
      };
      el.classList.add(`${classProcessed}`);
      // eslint-disable-next-line no-new
      new Swiper(el.querySelector(`.${className}__slider`), options);
    },
  );
};
