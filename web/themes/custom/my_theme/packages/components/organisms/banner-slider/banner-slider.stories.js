import { storiesOf } from '@storybook/html';
import './banner-slider.css';
import bannerSlider from './o-banner-slider';
import svg from '../../../../dist/svg/sprite.svg';


const template = require('./o-banner-slider.html.twig');
const data = require('./o-banner-slider.json');

const itemTemplate = require('../../molecules/banner/m-banner.html.twig');
const itemData = require('../../molecules/banner/m-banner.json');

itemData.modifier_class = 'o-banner-slider__item';

const item = itemTemplate(itemData);
data.content.items = [item, item, item];


// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('organisms|banner-slider', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      bannerSlider();
    },
    false,
  );
  return template({
    ...data,
    svg,
  });
});
