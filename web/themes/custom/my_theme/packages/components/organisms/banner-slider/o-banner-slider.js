/**
 * @file
 * This is Javascript slider functionality.
 */

import { Swiper, Navigation } from 'swiper/dist/js/swiper.esm';

Swiper.use([Navigation]);

const defaultOptions = {
  watchOverflow: true,
  navigation: {
    disabledClass: 'a-slider-arrow--disabled',
  },
};

export default ({
  className = 'o-banner-slider',
  classProcessed = 'o-banner-slider--processed',
  context = document,
  options = defaultOptions,
} = {}) => {
  Array.prototype.forEach.call(
    context.querySelectorAll(`.${className}:not(.${classProcessed})`),
    el => {
      options.navigation = {
        ...options.navigation,
        nextEl: el.querySelector(`.${className}__arrow--right`),
        prevEl: el.querySelector(`.${className}__arrow--left`),
      };
      el.classList.add(classProcessed);
      // eslint-disable-next-line no-new
      new Swiper(el, options);
    },
  );
};
