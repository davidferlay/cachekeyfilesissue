import { storiesOf } from '@storybook/html';
import './modal.css';
import svg from '../../../../dist/svg/sprite.svg';

const template = require('./o-modal.html.twig');
const data = require('./o-modal.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('organisms|modal', module).add('default', () =>
  template({
    ...data,
    svg,
  }),
);
