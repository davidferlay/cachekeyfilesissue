import { storiesOf } from '@storybook/html';
import './accordion.css';
import accordion from './m-accordion';

const template = require('./m-accordion.html.twig');
const data = require('./m-accordion.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|accordion', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      accordion();
    },
    false,
  );
  return template(data);
});

