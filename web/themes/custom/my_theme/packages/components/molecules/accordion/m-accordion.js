/**
 * @file
 * This is component sctipt template.
 */

const defaultOptions = {
  classes: {
    'ui-accordion-header': 'm-accordion__header',
    'ui-accordion-header-icon': 'm-accordion__icon',
    'ui-accordion-header-collapsed': 'm-accordion__header--collapsed',
    'ui-accordion-content': 'm-accordion__content',
  },
};

export default ({
  className = '.m-accordion',
  classProcessed = 'm-accordion--processed',
  context = document,
  $ = window.jQuery,
  options = defaultOptions,
} = {}) => {
  Array.prototype.forEach.call(
    context.querySelectorAll(`${className}:not(.${classProcessed})`),
    el => {
      el.classList.add(classProcessed);
      $(el).accordion(options);
    },
  );
};
