/**
 * @file
 * This is custom js for display datepicker in sb.
 */

const dateFormat = 'dd/mm/yy';
const defaultOptions = {
  numberOfMonths: 2,
  firstDay: 1,
  minDate: 0,
};

export default ({
  className = '.m-datepicker-example',
  classProcessed = 'm-datepicker-example--processed',
  context = document,
  options = defaultOptions,
  $ = window.jQuery,
} = {}) => {
  function getDate(element) {
    let date;
    try {
      date = $.datepicker.parseDate(dateFormat, element.value);
    } catch (error) {
      date = null;
    }
    return date;
  }
  Array.prototype.forEach.call(
    context.querySelectorAll(`${className}:not(.${classProcessed})`),
    el => {
      const $from = $('#from', el);
      const $to = $('#to', el);

      options.beforeShowDay = date => {
        const date1 = $.datepicker.parseDate(dateFormat, $from.val());
        const date2 = $.datepicker.parseDate(dateFormat, $to.val());
        if (date1 && date && date1.getTime() === date.getTime()) {
          return [true, 'daterange-start', ''];
        }
        if (date2 && date && date2.getTime() === date.getTime()) {
          return [true, 'daterange-end', ''];
        }

        if (date2 && date1 && date >= date1 && date <= date2) {
          return [true, 'date-selected-range', ''];
        }
        return [true, '', ''];
      };

      $from.datepicker(options).on('change', () => {
        $to.datepicker('option', 'minDate', getDate(this));
      });

      $to.datepicker(options).on('change', () => {
        $from.datepicker('option', 'maxDate', getDate(this));
      });
    },
  );
};
