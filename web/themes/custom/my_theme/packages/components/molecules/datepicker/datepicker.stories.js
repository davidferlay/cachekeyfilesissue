import { storiesOf } from '@storybook/html';
import './datepicker.css';
import datepicker from './m-datepicker';

const template = require('./m-datepicker.html.twig');
const data = require('./m-datepicker.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|datepicker', module).add('default', () => {
  document.addEventListener(
    'DOMNodeInserted',
    () => {
      datepicker();
    },
    false,
  );
  return template(data);
});

