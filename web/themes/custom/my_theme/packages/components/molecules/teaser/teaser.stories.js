import { storiesOf } from '@storybook/html';
import './teaser.css';

const template = require('./m-teaser.html.twig');
const data = require('./m-teaser.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('molecules|teaser', module)
  .add('default', () =>
    template({
      ...data,
      teaser_width: '250px;',
    }),
  )
  .add('secondary', () =>
    template({
      ...data,
      teaser_width: '250px;',
      modifier_class: 'm-teaser--secondary',
      content_button_modifier_class: 'm-teaser__content-button--secondary',
      button_modifier_class: 'a-button--no-round',
    }),
  )
  .add('banner', () =>
    template({
      ...data,
      teaser_width: '500px;',
      modifier_class: 'm-teaser--banner',
    }),
  );
