import { storiesOf } from '@storybook/html';
import './p-icon-label.css';

const template = require('./m-p-icon-label.html.twig');
const data = require('./m-p-icon-label.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|p-icon-label', module)
  .add('default', () =>
    template({
      ...data,
      modifier_class: 'm-p-icon-label',
    }),
  )
  .add('one line', () =>
    template({
      ...data,
      modifier_class: 'm-p-icon-label--flex',
      icon_wrapper_modifier_class: 'm-p-icon-label__icon-wrapper--less',
      icon_modifier_class: 'm-p-icon-label__icon--less',
      label_modifier_class: 'm-p-icon-label__label--bigger m-p-icon-label__label--after-icon',
    }),
  );

