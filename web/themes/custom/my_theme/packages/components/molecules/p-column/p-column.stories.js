import { storiesOf } from '@storybook/html';
import './p-column.css';

const template = require('./m-p-column.html.twig');
const data = require('./m-p-column.json');

storiesOf('molecules|p-column', module).add('default', () => template(data));
