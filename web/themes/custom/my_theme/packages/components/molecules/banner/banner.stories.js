import { storiesOf } from '@storybook/html';
import './banner.css';


const template = require('./m-banner.html.twig');
const data = require('./m-banner.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();


storiesOf('molecules|banner', module).add('default', () => template(data));

