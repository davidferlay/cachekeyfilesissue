export default ({
  className = '.m-media',
  classProcessed = 'm-media--processed',
  context = document,
} = {}) => {
  Array.prototype.forEach.call(
    context.querySelectorAll(`${className}:not(.${classProcessed})`),
    el => {
      el.classList.add(classProcessed);
      const iframe = el.querySelector('iframe');

      iframe.insertAdjacentHTML(
        'afterend',
        '<div class="m-media__overlay"><div class="m-media__overlay"></div></div>',
      );
    },
  );
};
