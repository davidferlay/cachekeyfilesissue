import { storiesOf } from '@storybook/html';
import './media.css';
import applyMedia from './media';

const template = require('./m-media.html.twig');
const data = require('./m-media.json');
const dataIframe = require('./m-media--iframe.json');
const dataIframeVimeo = require('./m-media--iframe--vimeo.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('molecules|media', module)
  .add('default', () => template(data))
  .add('iframe', () => {
    document.addEventListener(
      'DOMNodeInserted',
      () => {
        applyMedia();
      },
      false,
    );
    return template(dataIframe);
  })
  .add('iframe vimeo', () => {
    document.addEventListener(
      'DOMNodeInserted',
      () => {
        applyMedia();
      },
      false,
    );
    return template(dataIframeVimeo);
  });
