import { storiesOf } from '@storybook/html';
import './block.css';

const template = require('./m-block.html.twig');
const data = require('./m-block.json');

// Uncomment next 2 lines if your templates contains {{ attibutes.addClass(...) }} or similar logic.
// import drupalAttribute from 'drupal-attribute';
// data.attributes = new drupalAttribute();

storiesOf('molecules|block', module)
  .add('default', () => template(data))
  .add('secondary', () =>
    template({
      ...data,
      modifier_class: 'm-block--bg--secondary',
    }),
  );
